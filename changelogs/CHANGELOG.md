## 0.3.1 [07-06-2023]

* Auth and new call

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!4

---

## 0.3.0 [03-24-2023]

* Add tasks and oauth support

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!3

---