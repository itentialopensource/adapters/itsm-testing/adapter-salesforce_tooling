# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Salesforce_tooling System. The API that was used to build the adapter for Salesforce_tooling is usually available in the report directory of this adapter. The adapter utilizes the Salesforce_tooling API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Salesforce Tooling adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Salesforce Tooling. With this adapter you have the ability to perform operations such as:

- Retrieve metadata information about your Salesforce organization, including objects, fields, and layouts.

- Create, retrieve, update, or delete Apex classes, triggers, and other Apex code components.

- Manage Visualforce pages and components, allowing you to automate the creation or modification of custom UI components.

- Create, modify, or delete custom objects, fields, and relationships, which is particularly useful for customizing your Salesforce instance.

- Tooling API can be used to manage permission sets, enabling administrators to programmatically assign or revoke permissions for users.

- Create, update, and retrieve custom metadata types. This can help automate the setup of custom configurations.

- Upload and manage static resources, such as JavaScript, CSS, or images, which are used in Visualforce pages and Lightning components.

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
