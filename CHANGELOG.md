
## 0.9.4 [10-15-2024]

* Changes made at 2024.10.14_20:59PM

See merge request itentialopensource/adapters/adapter-salesforce_tooling!19

---

## 0.9.3 [09-16-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-salesforce_tooling!17

---

## 0.9.2 [08-14-2024]

* Changes made at 2024.08.14_19:21PM

See merge request itentialopensource/adapters/adapter-salesforce_tooling!16

---

## 0.9.1 [08-07-2024]

* Changes made at 2024.08.06_21:02PM

See merge request itentialopensource/adapters/adapter-salesforce_tooling!15

---

## 0.9.0 [08-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!14

---

## 0.8.6 [03-27-2024]

* Changes made at 2024.03.27_14:08PM

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!13

---

## 0.8.5 [03-21-2024]

* Changes made at 2024.03.21_14:40PM

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!12

---

## 0.8.4 [03-13-2024]

* Changes made at 2024.03.13_15:08PM

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!11

---

## 0.8.3 [03-11-2024]

* Changes made at 2024.03.11_14:48PM

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!10

---

## 0.8.2 [02-28-2024]

* Changes made at 2024.02.28_11:14AM

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!9

---

## 0.8.1 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!8

---

## 0.8.0 [12-14-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!5

---

## 0.7.0 [12-14-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!5

---

## 0.6.0 [11-07-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!5

---

## 0.5.0 [11-07-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!5

---

## 0.4.0 [11-07-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!5

---

## 0.3.1 [07-06-2023]

* Auth and new call

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!4

---

## 0.3.0 [03-24-2023]

* Add tasks and oauth support

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!3

---

## 0.2.0 [05-24-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!2

---

## 0.1.1 [03-14-2021]

- migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/itsm-testing/adapter-salesforce_tooling!1

---
