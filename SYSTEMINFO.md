# Adapter for Salesforce Tooling

Vendor: Salesforce
Homepage: https://www.salesforce.com/

Product: Tooling
Product Page: https://developer.salesforce.com/docs/atlas.en-us.api_tooling.meta/api_tooling/intro_api_tooling.htm

## Introduction
We classify Salesforce Tooling into the ITSM domain as Salesforce Tooling provides this capability to its users. It provides customer relationship management software and applications focused on sales, customer service, marketing automation, e-commerce, analytics, and application development.


## Why Integrate
The Salesforce Tooling adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Salesforce Tooling. With this adapter you have the ability to perform operations such as:

- Retrieve metadata information about your Salesforce organization, including objects, fields, and layouts.

- Create, retrieve, update, or delete Apex classes, triggers, and other Apex code components.

- Manage Visualforce pages and components, allowing you to automate the creation or modification of custom UI components.

- Create, modify, or delete custom objects, fields, and relationships, which is particularly useful for customizing your Salesforce instance.

- Tooling API can be used to manage permission sets, enabling administrators to programmatically assign or revoke permissions for users.

- Create, update, and retrieve custom metadata types. This can help automate the setup of custom configurations.

- Upload and manage static resources, such as JavaScript, CSS, or images, which are used in Visualforce pages and Lightning components.

## Additional Product Documentation
The [API documents for Salesforce Tooling](https://developer.salesforce.com/docs/atlas.en-us.api_tooling.meta/api_tooling/intro_rest_resources.htm)

